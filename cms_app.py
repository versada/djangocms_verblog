from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool


class VerblogApphook(CMSApp):
    name = "Verblog"
    urls = ["verblog.urls"]

apphook_pool.register(VerblogApphook)